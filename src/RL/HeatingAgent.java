/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

/**
 *
 * @author Adam
 */
public class HeatingAgent extends Agent
{

      public static int ActionToNumber(String name)
      {
	    if (name == "on")
	    {
		  return 1;
	    }
	    else
	    {
		  return 0;
	    }
      }

      String[] HeatingActionNames =
      {
	    "on", "off"
      };
      String[] HeatingTemperatureStateNames =
      {
	    //">5", ">1", ">H", ">-1", ">-5", "-5>"
	    "H>7", "H>5", "H>3", "H>1", ">H", "H>-1", "H>-3", "H>-5", "-7>H"
      };
      String[] HeatingTransformerStateNames =
      {
	    ">20", ">15", ">10", ">5", ">1", ">T", ">-1", ">-5", ">-10", ">-15", "-20>"
      };

      public HeatingAgent()
      {
	    Policy heatPolicy = new Policy();
	    heatPolicy.addToStateSpace(HeatingTemperatureStateNames, HeatingActionNames);
	    heatPolicy.name = "Heating";
	    policies.add(heatPolicy);
	    Policy transformerPolicy = new Policy();
	    transformerPolicy.addToStateSpace(HeatingTransformerStateNames, HeatingActionNames);
	    transformerPolicy.name = "TransHeat";
	    policies.add(transformerPolicy);
      }

      public void callUpdate(double temperature, double heatTarget,double transformerLoad, double targetLoad, double[] reward)
      {
	    /*if (temperature > Constants.target_temperature + 5)
	    {
		  temperature = 0;
	    }
	    else if (temperature > Constants.target_temperature + 1)
	    {
		  temperature = 1;
	    }
	    else if (temperature > Constants.target_temperature)
	    {
		  temperature = 2;
	    }
	    else if (temperature > Constants.target_temperature - 1)
	    {
		  temperature = 3;
	    }
	    else if (temperature > Constants.target_temperature - 5)
	    {
		  temperature = 4;
	    }
	    else
	    {
		  temperature = 5;
	    }*/
	    if (temperature > heatTarget + 7)
	    {
		  temperature = 0;
	    }
	    else if (temperature > heatTarget + 5)
	    {
		  temperature = 1;
	    }
	    else if (temperature > heatTarget + 3)
	    {
		  temperature = 2;
	    }
	    else if (temperature > heatTarget + 1)
	    {
		  temperature = 3;
	    }
	    else if (temperature > heatTarget)
	    {
		  temperature = 4;
	    }
	    else if (temperature > heatTarget - 1)
	    {
		  temperature = 5;
	    }
	    else if (temperature > heatTarget - 3)
	    {
		  temperature = 6;
	    }
	    else if (temperature > heatTarget - 5)
	    {
		  temperature = 7;
	    }
	    else
	    {
		  temperature = 8;
	    }

//do charge policy
	    double nextValueMax = policies.get(0).getMaxActionValue(HeatingTemperatureStateNames[(int) temperature]);
	    if (0 == winningPolicy)
	    {
		  policies.get(0).update(currentAction, reward[0], true, nextValueMax);
		  policies.get(0).currentState = HeatingTemperatureStateNames[(int) temperature];
	    }
	    else
	    {
		  policies.get(0).update(currentAction, reward[0], false, nextValueMax);
		  policies.get(0).currentState = HeatingTemperatureStateNames[(int) temperature];
	    }
//do transofrmer policy
	    // System.out.println("Trans= " + transformerLoad);
	    if (transformerLoad > targetLoad * 1.2)
	    {// ">5", 
		  transformerLoad = 0;
	    }
	    else if (transformerLoad > targetLoad * 1.15)
	    {// ">5", 
		  transformerLoad = 1;
	    }
	    else if (transformerLoad > targetLoad * 1.10)
	    {// ">5", 
		  transformerLoad = 2;
	    }
	    else if (transformerLoad > targetLoad * 1.05)
	    {// ">5", 
		  transformerLoad = 3;
	    }
	    else if (transformerLoad > targetLoad * 1.01)
	    {//  ">1",
		  transformerLoad = 4;
	    }
	    else if (transformerLoad > targetLoad)
	    {// ">T", 
		  transformerLoad = 5;
	    }
	    else if (transformerLoad > targetLoad * .99)
	    {//">-1",  
		  transformerLoad = 6;
	    }
	    else if (transformerLoad > targetLoad * .95)
	    {//">-5",
		  transformerLoad = 7;
	    }
	    else if (transformerLoad > targetLoad * .90)
	    {//">-5",
		  transformerLoad = 8;
	    }
	    else if (transformerLoad > targetLoad * .85)
	    {//">-5",
		  transformerLoad = 9;
	    }
	    else
	    {// "-5<"
		  transformerLoad = 10;
	    }
	    //System.out.println("Trans= " + HeatingTransformerStateNames[(int) transformerLoad] + " will be updated");
	    nextValueMax = policies.get(1).getMaxActionValue(HeatingTransformerStateNames[(int) transformerLoad]);
	    if (1 == winningPolicy)
	    {
		  policies.get(1).update(currentAction, reward[1], true, nextValueMax);
		  policies.get(1).currentState = HeatingTransformerStateNames[(int) transformerLoad];
	    }
	    else
	    {
		  policies.get(1).update(currentAction, reward[1], false, nextValueMax);
		  policies.get(1).currentState = HeatingTransformerStateNames[(int) transformerLoad];
	    }

      }

      public String getCurrentState()
      {
	    return policies.get(0).currentState + " " + policies.get(1).currentState;
      }

      public void printStateSpaceToConsole()
      {
	    System.out.println("Agent has " + policies.size() + " policies with values:");
	    for (int a = 0; a < policies.size(); a++)
	    {
		  System.out.println(policies.get(a).getString() + "\n");
	    }
      }
}
