/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RL;

import java.util.ArrayList;

/**
 *
 * @author Adam
 */
public class Policy
{

      boolean isExploring = true;
      private ArrayList<State> stateSpace;
      protected String currentState;
      public String name = "";

      public Policy()
      {
	    stateSpace = new ArrayList<>();
      }

      void addToStateSpace(String[] stateNames, String[] actionNames)
      {
	    for (int a = 0; a < stateNames.length; a++)
	    {
		  State toAdd = new State(stateNames[a]);
		  toAdd.addActions(actionNames);
		  stateSpace.add(toAdd);
	    }
      }

      Action selectAction()
      {
	    for (int a = 0; a < stateSpace.size(); a++)
	    {
		  if (stateSpace.get(a).getName() == currentState)
		  {//if the current state
			if (isExploring)
			{
			      return stateSpace.get(a).getExplorationAction();

			}
			else
			{
			      return stateSpace.get(a).getBestAction();
			}
		  }
	    }
	    System.err.println("Can't get an action from state: " + currentState);
	    System.exit(0);
	    return new Action("");
      }

      double getCurrentWValue()
      {
	    for (int a = 0; a < stateSpace.size(); a++)
	    {
		  if (stateSpace.get(a).getName() == currentState)
		  {//if the current state
			return stateSpace.get(a).getWValue();
		  }
	    }
	    return -999;
      }

      void update(Action incomingAction, double incommingReward, boolean b, double nextValueMax)
      {
	    for (int a = 0; a < stateSpace.size(); a++)
	    {
		  if (stateSpace.get(a).getName() == currentState)
		  {//if the correct state

			double actuallyUsedActionPreUpdatedQ = stateSpace.get(a).getAction(incomingAction.getName()).getQValue();
			stateSpace.get(a).getAction(incomingAction.getName()).updateQValue(incommingReward, nextValueMax);
			if (b == false)
			{//we lost update our w
			      stateSpace.get(a).updateWValue(incommingReward, actuallyUsedActionPreUpdatedQ, nextValueMax);
			}
			//System.out.println("updating " + stateSpace.get(a).getName() + " val= " + stateSpace.get(a).getAction(incomingAction.getName()).getQValue());
			break;

		  }

	    }

      }

      double getMaxActionValue(String stateName)
      {
	    for (int a = 0; a < stateSpace.size(); a++)
	    {
		  if (stateName == stateSpace.get(a).getName())
		  {
			return stateSpace.get(a).getMaxActionValue();
		  }
	    }
	    return -10;
      }

      public String getString()
      {
	    String output = "";
	    for (int a = 0; a < stateSpace.size(); a++)
	    {
		  output += stateSpace.get(a).getName() + " : " + stateSpace.get(a).printActionsToConsole() + "\n";
	    }
	    return output;
      }
}
