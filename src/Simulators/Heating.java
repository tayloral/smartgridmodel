/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulators;

/**
 *
 * @author Adam
 */
public class Heating
{

      /**
       * cooling side http://www.math.pitt.edu/~frank/0250/newton/newton/
       * -k(t-te) k~=3 for a well insulated house january temperature is
       * appoximated by 5 cos(pi t-12) t in hours so, temperature change =
       * -3(inside temp time 0 - 5 cos(pi time 0-12)/-3(inside temp time 1 - 5
       * cos(pi time 1-12) heating side also out +(in-out)*e^kt
       * https://www.quora.com/How-much-energy-does-it-take-to-raise-the-temperature-of-an-average-room-by-10-degrees
       * u=cp (spec heat of house) m(mass air density*volume) dtemp .9(air and
       * fudge factor for stuff) 1275 * 190*1 =218025 allow 8kw of heating
       * p=u/dtime time to heat 1 degree =27.3 min if dtime=15
       * u=15*8000=.9*1275*190*dtemp dtemp=.55 degrees in my house
       */
      double currentTemperature;
      double outsideTemprature;
      double maxTemperature = 30;
      private double power = 4000;
double localSetpoint;
      public Heating(int type)
      {
          if(type==2)
          {
              localSetpoint=Constants.target_temperature;
              power=12000;
          }
          else if(type==1)
          {
              localSetpoint=12;
          }
          else
          {
              localSetpoint=Constants.target_temperature;
          }
	    currentTemperature = 20;
      }
public double getTemperatureTarget()
{
    return this.localSetpoint;
}
      /**
       *
       * @param time how long for action
       * @param worldHour time hour
       * @param action 0 off 1 on
       */
      public double heat(double time, int worldHour, int action)
      {
	    outsideTemprature = 5 * Math.cos(Math.PI * (time - 12));
	    //System.out.println("outside =" + outsideTemprature);
	    double deltaT = -.27;

	    if (action == 1)
	    {//heat
		  deltaT += .55;
	    }
	    else
	    {//

	    }
	    currentTemperature += deltaT;
	    if (currentTemperature < outsideTemprature)
	    {
		  currentTemperature = outsideTemprature;
	    }
	    else if (currentTemperature > maxTemperature)
	    {
		  currentTemperature = maxTemperature;
	    }
	    if (action == 1)
	    {//heat
		  return this.power;
	    }
	    else
	    {//
		  return 0;
	    }
      }

      public double getReward()
      {
	    return -Math.abs(-currentTemperature + localSetpoint) * 200 / localSetpoint;
      }

      public double getCurrentTemperature()
      {
	    return currentTemperature;
      }

      public void setCurrentTemperature(double heatVar)
      {
	    currentTemperature = heatVar;
      }
}
