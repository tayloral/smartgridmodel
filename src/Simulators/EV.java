/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulators;

/**
 *
 * @author Adam
 */
public class EV
{

      /*Nissan leaf - https://www.nissan.co.uk/vehicles/new-vehicles/leaf/charging-range.html
      *either 24kwh or 30 drive 200km or 250
      *Rapid charge up to 80% in 30 mins,
      *charging unit 3.3kw - 7 hours or 9.5, 6.6kw - 4 hours or 5.5,
      *regular domestic charge cable 12-15 hours
       */
      private boolean isHighCapacity;
      private double charge;//in wh
      private double dailyDistance;//km
      private double whPerKM = .12 * 1000;
      private double slowCharge = 3200;//wh per hour
      private double fastCharge = 5600;
      private int awayHour = 9;
      private int homeHour = 17;
      private boolean isHome;
      

      public EV(boolean highCapacity,int type)
      {
           isHighCapacity = highCapacity;
          if(type==1)
          {
              awayHour=8;
              homeHour=16;
              isHighCapacity=false;
          }
          if(type==2)
          {
              awayHour=7;
              homeHour=18;
              isHighCapacity=true;
          }
	    isHome = true;
	   
	    if (isHighCapacity)
	    {
		  charge = Constants.highCapacityEV;
	    }
	    else
	    {
		  charge = Constants.lowCapacityEV;
	    }
	    dailyDistance = 70;
      }

      public void travel()
      {
	    double usage = dailyDistance * whPerKM;
	    //System.out.println("charge used = " + usage);
	    charge = charge - usage;
	    if (charge < 0)
	    {
		  charge = 0;
	    }
      }

      /**
       *
       * @param timePeriod in hours
       * @param chargeType 0 no 1 slow 2 fast
       * @return in watts
       */
      public double charge(double timePeriod, int chargeType)
      {
	    // System.out.println("Simulators.EV.charge() " + getCharge() + " type = " + chargeType);
	    double newPower;
	    if (isHome == false)
	    {
		  newPower = 0;
	    }
	    else if (chargeType == 2)
	    {
		  newPower = timePeriod * this.fastCharge;
	    }
	    else if (chargeType == 1)
	    {
		  newPower = timePeriod * this.slowCharge;
	    }
	    else if (chargeType == 0)
	    {
		  newPower = 0;
	    }
	    else
	    {
		  newPower = 0;
		  System.err.println("Wrong charge type");
		  System.exit(chargeType);
	    }
	    charge += newPower;
	    if (isHighCapacity)
	    {//fix over charging
		  if (charge > Constants.highCapacityEV)
		  {
			charge = Constants.highCapacityEV;
		  }
	    }
	    else if (charge > Constants.lowCapacityEV)
	    {
		  charge = Constants.lowCapacityEV;
	    }
	    // System.out.println(" now" + getCharge());
	    if (chargeType == 2)
	    {
		  return this.fastCharge;
	    }
	    else if (chargeType == 1)
	    {
		  return this.slowCharge;
	    }
	    else
	    {
		  return 0;
	    }

      }

      /**
       *
       * @return the charge as a %
       */
      public double getCharge()
      {
	    if (isHighCapacity)
	    {
		  return charge * 100 / Constants.highCapacityEV;
	    }
	    else
	    {
		  return charge * 100 / Constants.lowCapacityEV;
	    }
      }

      public double getReward()
      {
	    if (isHighCapacity)
	    {
		  return charge * 100 / Constants.highCapacityEV;
	    }
	    else
	    {
		  return charge * 100 / Constants.lowCapacityEV;
	    }
      }

      public void travel(int hour, int quater)
      {
	    if (quater == 0)
	    {
		  if (hour == awayHour)
		  {
			isHome = false;
		  }
		  else if (hour == homeHour)
		  {
			isHome = true;
			this.travel();
		  }
	    }
      }

      /**
       *
       * @param chargeVar as a %
       */
      public void setCharge(double chargeVar)
      {
	    if (isHighCapacity)
	    {
		  charge = chargeVar / 100 * Constants.highCapacityEV;
	    }
	    else
	    {
		  charge = chargeVar / 100 * Constants.lowCapacityEV;
	    }
      }

}
