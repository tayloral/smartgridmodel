/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulators;

/**
 *
 * @author Adam
 */
public class Clock
{

      private boolean slowTime;
      private int currentHour;
      private int currentQuater;
      private long lastTickTime;
      private int daysElapsed;
      private boolean paused;
      private long lastTickRemainingAfterPause;
      private boolean hasBeenUnPaused;

      public Clock()
      {
	    slowTime = false;
	    currentHour = 0;
	    currentQuater = 0;
	    lastTickTime = System.currentTimeMillis();
	    daysElapsed = 0;
	    paused = false;
	    hasBeenUnPaused = false;
      }

      public boolean tick()
      {
	    if (paused)
	    {
		  return false;
	    }
	    if (slowTime == true && System.currentTimeMillis() < lastTickTime + Constants.slowProgress)
	    {
		  return false;
	    }
	    lastTickTime = System.currentTimeMillis();
	    currentQuater++;
	    if (currentQuater >= 4)
	    {
		  currentQuater = 0;
		  currentHour++;
		  if (currentHour >= 24)
		  {
			currentHour = 0;
			daysElapsed++;
		  }
	    }

	    return true;
      }

      /**
       * @return the slowTime
       */
      public boolean isSlowTime()
      {
	    return slowTime;
      }

      /**
       * @param slowTime the slowTime to set
       */
      public void setSlowTime(boolean slowTime)
      {
	    this.slowTime = slowTime;
	    lastTickTime = System.currentTimeMillis()-Constants.slowProgress;
      }

      public int getHour()
      {
	    return currentHour;
      }

      public int getQuater()
      {
	    return currentQuater;
      }

      public int getDaysElaspsed()
      {
	    return daysElapsed;
      }

      public void pause()
      {
	    if (paused == true)
	    {//unpause
		  paused = false;
		  lastTickTime = System.currentTimeMillis() - Constants.slowProgress + lastTickRemainingAfterPause;
	    }
	    else
	    {
		  paused = true;
		  lastTickRemainingAfterPause = lastTickTime + Constants.slowProgress - System.currentTimeMillis();
	    }
      }

      public boolean getPaused()
      {
	    return paused;
      }

      public String humanReadableTime()
      {
	    return "" + currentHour + ":" + currentQuater * 15;
      }
}
