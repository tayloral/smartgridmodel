/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 *
 * @author Adam
 */
class ModelWindow extends JFrame
{
      
      JTabbedPane tabbedPane;
      ManagementView managementView;
      SimulationView simulationView;
      Dimension size;
     public static MessageSender messegeSender;//no need for therad safty as only one view at a time can send data
      
      public ModelWindow()
      {
	    super();
            messegeSender=new MessageSender();
	    size = new Dimension(1250, 580);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setMinimumSize(size);
	    this.setPreferredSize(size);
	    this.addComponentListener(new ComponentAdapter()
	    {
		  public void componentResized(ComponentEvent evt)
		  {
			Dimension size = getSize();
			Dimension min = getMinimumSize();
			if (size.getWidth() < min.getWidth())
			{
			      setSize((int) min.getWidth(), (int) size.getHeight());
			      
			}
			if (size.getHeight() < min.getHeight())
			{
			      setSize((int) size.getWidth(), (int) min.getHeight());
			}
		  }
	    });

	    //make stuff
	    tabbedPane = new JTabbedPane();

	    //add to frame
	    this.getContentPane().add(tabbedPane);
	    managementView = new ManagementView(tabbedPane.getSize());
	    simulationView = new SimulationView(tabbedPane.getSize());
	    tabbedPane.addTab("Manage", managementView);
	    tabbedPane.addTab("Simulation", simulationView);
	    tabbedPane.setSelectedIndex(1);
	    this.setVisible(true);
      }
      
}
