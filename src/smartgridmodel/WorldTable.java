/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartgridmodel;

import RL.Action;
import java.awt.Color;
import java.awt.GridLayout;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Adam
 */
public class WorldTable extends JPanel implements Runnable
{
      final String DEGREE = "\u00b0";
      private ArrayList<Double> evCharges;
      private ArrayList<Action> evActions;
      private ArrayList<Double> heatTemperatures;
      private ArrayList<Action> heatActions;
      JLabel[] nameLabels;
      JLabel[] heatLabels;
      JLabel chargeTitle;
      JLabel nameTitle;
      JLabel heatTitle;
      JLabel[] chargeLabels;
      int dataToShow = 0;

      public WorldTable()
      {
	    evActions = new ArrayList<>();
	    heatActions = new ArrayList<>();
	    evCharges = new ArrayList<>();
	    heatTemperatures = new ArrayList<>();
	    GridLayout gridLayout = new GridLayout(Constants.housesInSimulation + 1, 3);
	    nameLabels = new JLabel[Constants.housesInSimulation];
	    heatLabels = new JLabel[Constants.housesInSimulation];
	    chargeLabels = new JLabel[Constants.housesInSimulation];
	    heatTitle = new JLabel("<HTML><U>Temperature</U></HTML>");
	    nameTitle = new JLabel("<HTML><U>House</U></HTML>");
	    chargeTitle = new JLabel("<HTML><U>Charge</U></HTML>");
	    this.setLayout(gridLayout);
	    gridLayout.setHgap(10);
	    this.add(nameTitle);
	    this.add(heatTitle);
	    this.add(chargeTitle);
	    for (int a = 0; a < Constants.housesInSimulation; a++)
	    {
		  nameLabels[a] = new JLabel("House n");
		  heatLabels[a] = new JLabel("99" + DEGREE + "C");
		  chargeLabels[a] = new JLabel("000%");
		  nameLabels[a].setOpaque(true);
		  heatLabels[a].setOpaque(true);
		  chargeLabels[a].setOpaque(true);
		  this.add(nameLabels[a]);
		  this.add(heatLabels[a]);
		  this.add(chargeLabels[a]);
	    }
      }

      @Override
      public void run()
      {
	    try
	    {

		  if (dataToShow > 3 && evActions.size() >= Constants.housesInSimulation && evCharges.size() >= Constants.housesInSimulation && heatActions.size() >= Constants.housesInSimulation && heatTemperatures.size() >= Constants.housesInSimulation)
		  {
			//int setRed = 0;
			//int grey = 0;
			for (int a = 0; a < Constants.housesInSimulation; a++)
			{
			      nameLabels[a].setText("House " + (a + 1));

			      BigDecimal bd = new BigDecimal(heatTemperatures.get(a));
			      bd = bd.round(new MathContext(4));
			      heatLabels[a].setText(bd.doubleValue() + DEGREE + "C");
			      bd = new BigDecimal(evCharges.get(a));
			      bd = bd.round(new MathContext(4));
			      chargeLabels[a].setText(bd.doubleValue() + "%");
			      if (evActions.get(a).getName() == "off")
			      {
				    chargeLabels[a].setBackground(Color.gray);
				    chargeLabels[a].setForeground(Color.black);
                                    
                                    ModelWindow.messegeSender.sendMessage(a+1, Integer.MIN_VALUE, Constants.Command.off, Constants.wemosColourStrings[Constants.evColour]);
			      }
			      else if (evActions.get(a).getName() == "slow")
			      {
				    chargeLabels[a].setBackground(Color.cyan);
				    chargeLabels[a].setForeground(Color.black);
                                    ModelWindow.messegeSender.sendMessage(a+1, Integer.MIN_VALUE, Constants.Command.flash, Constants.wemosColourStrings[Constants.evColour]);     
			      }
			      else
			      {
				    chargeLabels[a].setBackground(Color.blue);
				    chargeLabels[a].setForeground(Color.white);
                                    ModelWindow.messegeSender.sendMessage(a+1, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[Constants.evColour]);
			      }
			      if (heatActions.get(a).getName() == "off")
			      {
				    heatLabels[a].setBackground(Color.gray);
                                    ModelWindow.messegeSender.sendMessage(a+1, Integer.MIN_VALUE, Constants.Command.off, Constants.wemosColourStrings[Constants.heatColour]);
				    //grey++;
			      }
			      else
			      {
                                  
				    // setRed++;
				    heatLabels[a].setBackground(Color.red);
                                    ModelWindow.messegeSender.sendMessage(a+1, Integer.MIN_VALUE, Constants.Command.on, Constants.wemosColourStrings[Constants.heatColour]);
			      }
			      // System.out.println(a + " " + heatActions.get(a).getName());
			}
			//System.out.println("REd= " + setRed + " grey= " + grey);
			this.repaint();
			dataToShow = 0;
		  }
	    }
	    catch (Exception e)
	    {
		  System.out.println(e.getLocalizedMessage());
		  System.out.println(evActions.size());
		  e.printStackTrace();
	    }
      }

      public void setEVCharges(ArrayList<Double> input)
      {
	    if (input == null)
	    {

	    }
	    else
	    {
		  dataToShow++;
		  evCharges = input;
	    }
      }

      public void setEVActions(ArrayList<Action> input)
      {
	    if (input == null)
	    {

	    }
	    else
	    {
		  dataToShow++;
		  evActions = input;
	    }
      }

      public void setHeatTemperatures(ArrayList<Double> input)
      {
	    if (input == null)
	    {

	    }
	    else
	    {
		  dataToShow++;
		  heatTemperatures = input;
	    }
      }

      public void setHeatActions(ArrayList<Action> input)
      {
	    if (input == null)
	    {

	    }
	    else
	    {
		  dataToShow++;
		  heatActions = input;
	    }
      }
}
